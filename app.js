const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const port = 3000;//process.env.PORT;
const router = express.Router();
const notes = require("./controllers/note.controller.js");

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));
app.use('/',router);

router.use((req, res, next) => {
  console.log("route midd");
  next();
})

router.route("/notes")
  	.get(notes.index)
  	.post(notes.store);

router.route("/notes/:id")
  	.get(notes.show)
  	.put(notes.update)
  	.delete(notes.destroy)

router.get("/", (req, res) => {
  res.send('<a href="https://www.openode.io">thanks www.openode.io.</a>');
  setTimeout(()=>{console.log("jejeberns")},10000);
});

//require("./configs/routes.js")(app);

app.listen(port,function(){
	console.log(`Corriendo en el puerto ${port}.`);
});