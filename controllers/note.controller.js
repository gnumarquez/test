"use strict";
const Note = require("../models/note.model.js");

exports.index = async (req,res) =>{
	const rows = await Note.find();
	res.json({success:Array.isArray(rows),data:rows});
}

exports.show = async (req,res) =>{
	const rows = await Note.find(req.params.id);
	res.json({success:Array.isArray(rows),data:rows});
}

exports.store = async (req, res) => {
	const note = new Note(req.body);
	const ob = await note.save();
	res.json({success:typeof(ob)=="object",data:ob});
};

exports.update = (req, res) => {
	let result,note;
	note = new Note(req.body);
	note.save(req.params.id)
		.then(rows=>{
			result = {success:true,data:rows}
		}).catch(err=>{
			result = {success:false,message:err}
		}).finally(()=>{
			res.json(result)
		});
};

exports.destroy = (req,res) =>{
	let result;
	new Note().delete(req.params.id)
		.then(rows=>{
			result = {success:true,data:rows}
		}).catch(err=>{
			result = {success:false,message:err}
		}).finally(()=>{
			res.json(result)
		});
}
