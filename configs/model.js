const db = require('../configs/bd');

class Model {
	constructor(data) {
		this._table = `${this.constructor.name.toLowerCase()}`;
		this._data = data;
	}
	save(id){
		let sql;
		return new Promise(async (resolve,reject)=>{
			if (id === undefined) {
				sql = [`INSERT INTO ${this._table} SET ?`,[this._data]];
			} else {
				sql = [`UPDATE ${this._table} SET ? where id = ?`,[this._data,id]];
			}
			db.query(...sql,(err,res)=>{
				if (err) { 
					resolve("Error al guardar los datos.");
					return; 
				};
				if (id === undefined) {
					resolve({id:res.insertId,...this._data})
				} else {
					if (res.affectedRows>0) {
						resolve({id:id,...this._data});
					} else {
						resolve("No actualizado.");
					}
				}
				
			});

		})
		
	}
	static find(id){
		let sql;
		return new Promise(async (resolve,reject)=>{
			if (id===undefined) {
				sql = [`SELECT * FROM ${this.name}`];
			} else {
				sql = [`SELECT * FROM ${this.name} where id =? limit 1`,[id]];
			}
			db.query(...sql,(err,res)=>{
				if (err) { 
					resolve("Error al ejecutar la consulta.");
					return; 
				};
				res.forEach(val=>{
					this._hidden.forEach(field=>{
						delete val[field];
					});
				});
				resolve(res);
			})
		})
}
	delete(id){
		return new Promise(async (resolve,reject)=>{
			if (typeof(id)==="undefined") {
				reject("El campo id no puede estar vacío.");
			}
			db.query(`DELETE FROM ${this._table} where id =?`,[id],(err,res)=>{
				if (err) { 
					reject("Error al ejecutar la consulta.");
					return; 
				};
				if (res.affectedRows>0) {
					resolve("Eliminado.");
				} else {
					reject("No eliminado.");
				}
				
			})
		})
		
	}
}
module.exports = Model;
	