'use strict';
const mysql = require('mysql2');

const Pool = mysql.createPool({
	  host            : 'localhost',
	  user            : 'root',
	  password        : '',
	  database        : 'notes',
	  charset 		  : 'utf8'
	});

module.exports = Pool;